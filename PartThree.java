import java.util.Scanner;
public class PartThree{
  
  public static void main(String[] args){
    Scanner sc = new Scanner(System.in);
    AreaComputations ac = new AreaComputations();
    
    System.out.println("What is the side of the square ?");
    int side = sc.nextInt();
    System.out.println("What is the length of the rectangle ?");
    int length = sc.nextInt();
    System.out.println("What is the width of the rectangle ?");
    int width = sc.nextInt();
    
    System.out.println("The area of the square is " + AreaComputations.areaSquare(side) + " .");
    System.out.println("The area of the rectangle is " + ac.areaRectangle(length, width) + " .");
            
  }
  
  
}